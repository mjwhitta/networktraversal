/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Sep 10, 2011
 */

package sites.mjwhitta.networktraversal.impl;

public class Edge {
   private final Vertex m_first;

   private final Vertex m_last;

   private final int m_weight;

   public Edge(Vertex first, Vertex last, int weight) {
      m_first = first;
      m_last = last;
      m_weight = weight;
   }

   public Vertex getCurrentVertex() {
      return m_first;
   }

   public Vertex getDestinationVertex() {
      return m_last;
   }

   public int getWeight() {
      return m_weight;
   }

   public Edge reverse() {
      return new Edge(m_last, m_first, m_weight);
   }
}
