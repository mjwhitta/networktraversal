/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Sep 13, 2011
 */

package sites.mjwhitta.networktraversal.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShortestPath {
   private static final int INFINITY = Integer.MAX_VALUE;

   private final Vertex m_vertex;

   private final Map<Object, Path> m_shortestPaths = new HashMap<Object, Path>();

   public ShortestPath(Vertex v, ArrayList<Vertex> vertices) {
      m_vertex = v;
      calcShortestPaths(vertices);
   }

   private void calcShortestPaths(ArrayList<Vertex> vertices) {
      // add all vertices to shortest path map and set weights to infinity
      for (Vertex v : vertices) {
         m_shortestPaths.put(v.getId(), new Path(m_vertex));
         m_shortestPaths.get(v.getId()).setUnreachable();
      }

      // set root vertex's weight to 0
      m_shortestPaths.remove(m_vertex.getId());
      m_shortestPaths.put(m_vertex.getId(), new Path(m_vertex));

      // while there are vertices left to add
      while (!vertices.isEmpty()) {
         // get next vertex with minimum weight
         Vertex minVertex = getVertexWithMinWeight(vertices);

         // if weight is infinity then all reachable vertices have been
         // added so break out of loop
         if (getShortestPathWeight(minVertex) == INFINITY) {
            break;
         }

         // remove vertex with minimum weight from list vertices that still
         // need added
         vertices.remove(minVertex);

         // check neighbors that still need to be added to see if there
         // exists a shorter path than one that has already been found
         for (Vertex v : minVertex.getNeighbors()) {
            if (vertices.contains(v)) {
               int newWeight = (getShortestPathWeight(minVertex) + minVertex
                     .getEdge(v).getWeight());
               int oldWeight = getShortestPathWeight(v);

               if (newWeight < oldWeight) {
                  // update shortest path
                  Path p = new Path(m_shortestPaths.get(minVertex.getId()));
                  m_shortestPaths.remove(v.getId());
                  p.addVertex(v, minVertex.getEdge(v).getWeight());
                  m_shortestPaths.put(v.getId(), p);
               }
            }
         }
      }
   }

   public Map<Object, Path> getShortestPaths() {
      return m_shortestPaths;
   }

   private int getShortestPathWeight(Vertex v) {
      return m_shortestPaths.get(v.getId()).getWeight();
   }

   private Vertex getVertexWithMinWeight(ArrayList<Vertex> vertices) {
      Vertex minVertex = vertices.get(0);
      for (Vertex v : vertices) {
         int thisWeight = m_shortestPaths.get(v.getId()).getWeight();
         int minWeight = m_shortestPaths.get(minVertex.getId()).getWeight();

         if (thisWeight < minWeight) {
            minVertex = v;
         }
      }

      return minVertex;
   }
}
