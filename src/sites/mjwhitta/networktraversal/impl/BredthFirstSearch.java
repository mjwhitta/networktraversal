/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Sep 10, 2011
 */

package sites.mjwhitta.networktraversal.impl;

import java.util.ArrayList;
import java.util.List;

public class BredthFirstSearch {
   /**
    * Iterative bfs using a FIFO queue
    * 
    * @param v
    */
   public static void traverse(Vertex v) {
      // initialize FIFO
      List<Vertex> fifo = new ArrayList<Vertex>();

      // add first vertex
      fifo.add(v);

      // while FIFO isn't empty
      while (!fifo.isEmpty()) {
         // get front of FIFO
         Vertex curr = fifo.remove(0);

         // print
         System.out.println(curr.toString());

         // mark visited
         curr.markVisited();

         // add neighbors to FIFO if they haven't already been visited
         // add in order to visit in order
         for (Edge e : curr.getEdges()) {
            if (!e.getDestinationVertex().isVisited()) {
               fifo.add(e.getDestinationVertex());
            }
         }
      }
   }
}
