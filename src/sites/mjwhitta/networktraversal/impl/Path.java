/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Sep 14, 2011
 */

package sites.mjwhitta.networktraversal.impl;

import java.util.ArrayList;
import java.util.List;

public class Path {
   private static final int INFINITY = Integer.MAX_VALUE;

   List<Vertex> m_path = new ArrayList<Vertex>();

   private int m_weight = 0;

   public Path(Path p) {
      m_path = new ArrayList<Vertex>(p.m_path);
      m_weight = p.m_weight;
   }

   public Path(Vertex v) {
      m_path.add(v);
   }

   public void addVertex(Vertex v, int weight) {
      m_path.add(v);
      m_weight += weight;
   }

   public int getWeight() {
      return m_weight;
   }

   public void setUnreachable() {
      m_weight = INFINITY;
   }

   @Override
   public String toString() {
      String toRet = "";

      for (int i = 0; i < (m_path.size() - 1); ++i) {
         toRet += m_path.get(i).getId() + " -> ";
      }
      if (m_path.size() != 0) {
         toRet += m_path.get(m_path.size() - 1).getId() + " : " + m_weight;
      }

      return toRet;
   }
}
