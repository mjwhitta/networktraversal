/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Sep 10, 2011
 */

package sites.mjwhitta.networktraversal.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Vertex {
   private final int m_id;

   private final List<Edge> m_edges = new ArrayList<Edge>();

   private final List<Vertex> m_neighbors = new ArrayList<Vertex>();

   private boolean m_visited;

   private ShortestPath m_shortestPaths = null;

   public Vertex(int id) {
      m_id = id;
   }

   public void addEdge(Edge edge) {
      m_edges.add(edge);
      m_neighbors.add(edge.getDestinationVertex());
   }

   public void calcShortestPaths(ArrayList<Vertex> vertices) {
      m_shortestPaths = new ShortestPath(this, vertices);
   }

   public void clearVisited() {
      m_visited = false;
   }

   public Edge getEdge(Vertex v) {
      for (Edge e : m_edges) {
         if (v == e.getDestinationVertex()) {
            return e;
         }
      }
      return null;
   }

   public List<Edge> getEdges() {
      return m_edges;
   }

   public int getId() {
      return m_id;
   }

   public List<Vertex> getNeighbors() {
      return m_neighbors;
   }

   public Path getShortestPath(Vertex destination, ArrayList<Vertex> vertices) {
      if (m_shortestPaths == null) {
         calcShortestPaths(vertices);
      }

      return m_shortestPaths.getShortestPaths().get(destination.getId());
   }

   public Map<Object, Path> getShortestPaths(ArrayList<Vertex> vertices) {
      if (m_shortestPaths == null) {
         calcShortestPaths(vertices);
      }

      return m_shortestPaths.getShortestPaths();
   }

   public boolean isVisited() {
      return m_visited;
   }

   public void markVisited() {
      m_visited = true;
   }

   @Override
   public String toString() {
      String toRet = m_id + "   ";
      for (Edge e : m_edges) {
         toRet += e.getCurrentVertex().getId() + "-"
               + e.getDestinationVertex().getId() + " ";
      }

      return toRet;
   }
}
