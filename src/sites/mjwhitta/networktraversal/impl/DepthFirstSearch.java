/**
 * GNU GENERAL PUBLIC LICENSE See the file Launcher.java for copying conditions.
 * 
 * @author mjwhitta
 * @created Sep 10, 2011
 */

package sites.mjwhitta.networktraversal.impl;

import java.util.ArrayList;
import java.util.List;

public class DepthFirstSearch {
   /**
    * Iterative dfs using a stack
    * 
    * @param v
    */
   public static void traverse(Vertex v) {
      List<Vertex> stack = new ArrayList<Vertex>();

      stack.add(v);
      while (!stack.isEmpty()) {
         // remove top of stack
         Vertex curr = stack.remove(stack.size() - 1);

         // print
         System.out.println(curr.toString());

         // mark visited
         curr.markVisited();

         // add neighbors to stack if they haven't already been visited
         // add in reverse order so they are popped from stack in order
         for (int i = curr.getEdges().size() - 1; i >= 0; --i) {
            Edge e = curr.getEdges().get(i);
            if (!e.getDestinationVertex().isVisited()) {
               stack.add(e.getDestinationVertex());
            }
         }
      }
   }

   /**
    * Recursive dfs
    * 
    * @param v
    */
   public static void traverseRecursive(Vertex v) {
      // print
      System.out.println(v.toString());

      // mark visited
      v.markVisited();

      // call dfs for all neighbors that haven't already been visited
      // call in order so they are visited in order
      for (Edge e : v.getEdges()) {
         if (!e.getDestinationVertex().isVisited()) {
            traverseRecursive(e.getDestinationVertex());
         }
      }
   }
}
